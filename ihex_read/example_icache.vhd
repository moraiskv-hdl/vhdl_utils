-------------------------------------------------
-- Author:	Kevin P.M.
-- Date:	23 october, 2019
-- E-mail:  moraiskv@gmail.com
-------------------------------------------------
-- Title: 	iHex Reader Example
-- Description: a simple example of the ihex_reader_pkg.
-- It's used to fill a instruction cache, with direct mapping.
-- Each address of the cache stores a valid bit, and a 32 bits
-- instruction.
-------------------------------------------------
-- OBS.: the memory is considered to be 4-bytes aligned, thus,
--  it has a 2 bits offset at each address. The address access
-- is 12 bits long, as a result the tag is 32-12-2 = 18 bits long.
-- Data an Address length are defined in the generics 'n_data' and 'n_addr'.
--
-- OBS.2: this cache stores only the data, ignoring the tag and valid bit, wich
-- should be store on a extra memory for further verification (but this is 
-- only an example of the package usage). Valid and Addres info are used here
-- to select the line to be writen in to the cache, when found valid data.
-------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use std.textio.all;

use work.ihex_reader_pkg.all;

entity icache is
    generic(n_data : integer := 32;
            n_addr : integer := 12);
    port(
		clk		: in std_logic;                             -- clk input
		en		: in std_logic;                             -- enable input (high)
		we		: in std_logic;                             -- write enable input (high)
		addr	: in std_logic_vector(n_addr-1 downto 0);   -- address input
		din		: in std_logic_vector(n_data-1 downto 0);   -- data input
        dout	: out std_logic_vector(n_data-1 downto 0)); -- data output
end entity icache;

architecture behavior of icache is

	------------------------------------
	-- memory sections
	constant	TEXT_start	:	std_logic_vector(31 downto 0) := x"0000_0000";
	constant	TEXT_end	:	std_logic_vector(31 downto 0) := x"0010_FFFF";
	constant	SDATA_start	:	std_logic_vector(31 downto 0) := x"0011_0000";
	constant	SDATA_end	:	std_logic_vector(31 downto 0) := x"FFFE_FFFF";

	------------------------------------
	-- cache type
    type cache_type is ARRAY (0 to (2**n_addr)-1) of std_logic_vector(n_data-1 downto 0);

	------------------------------------
    -- cache initial value through a constant inside 'ihex_reader_pkg'
    -- this function reads the array output from the package, checking for valid bits, address (to
    -- form tags inside the cache, and data)
	impure function InitCacheFromFile return cache_type is
		-- mem type, define in the package
		variable	mem_word	:	std_logic_vector(mem_n-1 downto 0); -- valid:address:data
		variable	mem_valid	:	std_logic;
		variable	mem_addr	:	std_logic_vector(mem_naddr-1 downto 0);
		variable	mem_byte	:	std_logic_vector(7 downto 0);
		-- others
		variable	index1		:	integer := 0;
		variable	index2		:	integer := 0;
		-- output
		constant	offset		:	integer := 2;
		variable	cache		:	cache_type := (others => (others => '0'));
		variable	cache_index	:	std_logic_vector(n_addr-1 downto 0);
		constant	n_tag		:	integer := 32-n_addr-offset;	-- 2 bits offset address
		variable	cache_tag	:	std_logic_vector(n_tag-1 downto 0);
		variable	cache_word	:	std_logic_vector(n_data-1 downto 0) := (others => '0');
	begin
		for i in mem_type'range loop

			mem_word := (others => '0');	-- empty first
			mem_word := ihex_mem(i);        -- read an array line, and separate each info (valid, address, and data)
			mem_valid:= mem_word(mem_n-1);
			mem_addr := mem_word(mem_n-2 downto 8);
			mem_byte := mem_word(7 downto 0);

            if (mem_valid = '1' AND mem_addr <= TEXT_end) then
                -- if the array line is valid, and the address is inside the cache section, create the tag, and
                -- the cache index, according to the line address
				cache_index	:=	mem_addr(n_addr-1+offset downto 0+offset);
				cache_tag	:=	mem_addr(n_tag+n_addr-1+offset downto n_addr+offset);

				index1		:= to_integer(unsigned(cache_index));
				index2		:= to_integer(unsigned(mem_addr(1 downto 0)));

				cache(index1)(7+index2*8 downto 0+index2*8) := mem_byte;
			else
				null;
			end if;
		
		end loop;
	
        return cache;
    end function;

    signal cache : cache_type := InitCacheFromFile;

------------------------------------
------------------------------------
begin

	process(clk)
	begin
		if(rising_edge(clk)) then
			if (en = '1') then
				-------------------------
				-- write first
				if (we = '1') then
					cache(to_integer(unsigned(addr))) <= din;
					dout <= din;
				else
					dout <=	cache(to_integer(unsigned(addr)));
				end if;
				-------------------------
			end if;
		end if;
	end process;

end architecture behavior;
