-- *****************************************************************************
-- Author:  Kevin Paula Morais
-- Date:    23/08/2019
-- E-mail:  moraiskv@gmail.com
-- *****************************************************************************
-- File:    ihex_init_pkg.vhd
-- Description: functions to open a intel hexadecimal
-- file, read it's content, and output in to a variable.
--
-- The output it's an array, with the following info at each address:
--    valid (1 bit) : data_address (32 bits) : data (8 bits)
--
--    valid => when '1', it means the data read from the ihex file is valid,
--        otherwise, this line (address) of the array should be ignored
--    data_address => a 32 bits address, corresponding to the data address
--        of the data read (if valid)
--    data => data byte read from the ihex file
--
-- How to use:
-- 1) set the file path in to a constant string called 'IHEX_INIT_FILEPATH':
-- constant IHEX_INIT_FILEPATH : string := "/..../path/main.hex";
--
-- 2) inform the maximum possible value of memory used by the ihex file,
-- through the constant 'kb_ram', defined in kbytes
--
-- 3) a constant called 'IHEX_MEM_OUTPUT' is started with the ihex code, inside this package,
-- ready to be used
--
-- 4) the way to be used the output array (IHEX_MEM_OUTPUT) with the valid:address:data info,
-- is according to the project, an example where a simple instruction cache have
-- it's address filled by the ihex code should be together with this package
-- *****************************************************************************

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use std.textio.all;

use work.string_pkg.all;  -- string <-> stdlogic_vec convert

-- *****************************************************************************
package ihex_init_pkg is

  -- ***************************************************************************
  -- Memory to be used and iHex file information
  constant KB_BRAM    : integer := 2;                   -- each Block RAM has 2kB
  constant N_BRAM     : integer := 16;                  -- num of Block RAMs, each has 2kB
  constant MEM_SIZE   : integer := N_BRAM*KB_BRAM*1024; -- in bytes
  constant MEM_NVALID : integer := 1;                   -- MEM_NVALID, valid bit
  constant MEM_NADDR  : integer := 32;                  -- MEM_NADDR, address length
  constant MEM_NDATA  : integer := 8;                   -- MEM_NDATA, data length
  constant MEM_N      : integer := MEM_NVALID+MEM_NADDR+MEM_NDATA;

  -- data memory, keeps the ihex file data: valid:address:data
  type      mem_type is array (0 to MEM_SIZE-1) of std_logic_vector(MEM_N-1 downto 0);

  -- ihex file format constants
  constant TT_DATA : string(1 to 2) := "00";  --data
  constant TT_EOF  : string(1 to 2) := "01";  --end of file
  constant TT_ESA  : string(1 to 2) := "02";  --extended segment address
  constant TT_SSA  : string(1 to 2) := "03";  --start segment address
  constant TT_ELA  : string(1 to 2) := "04";  --extended linear address
  constant TT_SLA  : string(1 to 2) := "05";  --start linear address

  -- ***************************************************************************
  -- Read a  iHex file and outputs its information
  impure function
  read_ihex (ihex_filename : in string)
  return mem_type;

  impure function
  ihex_tt	(addr_in : in std_logic_vector; TT_string : in string)
  return std_logic_vector;

  -- Constant with the ihex file output
  constant IHEX_MEM_OUTPUT : mem_type := read_ihex(IHEX_INIT_FILE_PATH);

end package ihex_init_pkg;

-- *****************************************************************************
package body ihex_init_pkg is

  -- ***************************************************************************
  -- Read a iHex file in to a RAM with 1byte per address
  impure function read_ihex (ihex_filename: in string) return mem_type is
    FILE     ihexfile      : text is in ihex_filename;
    variable ihexfile_line : line;
    -- line data
    variable line_char     : character;
    variable line_stdv     : std_logic_vector(3 downto 0);
    variable line_str      : string(1 to 400) := (others => ' ');
    variable line_length   : integer := 0;
    variable line_count    : integer := 0;
    variable byte_count    : integer := 0;
    -- ihex fields
    variable LL_stdv       : std_logic_vector(7 downto 0) := (others => '0');
    variable LL            : integer := 0;
    variable AAAA_ext      : std_logic_vector(31 downto 0) := (others => '0');
    variable AAAA_stdv     : std_logic_vector(15 downto 0) := (others => '0');
    variable AAAA_lower    : std_logic_vector(15 downto 0) := (others => '0');
    variable AAAA          : std_logic_vector(31 downto 0) := (others => '0');
    variable TT_string     : string(1 to 2) := "00";
    variable DD_stdv       : std_logic_vector(7 downto 0) := (others => '0');
    -- mem type
    variable mem           : mem_type := (others => (others => '0'));
    variable mem_line      : std_logic_vector(MEM_N-1 downto 0);
  begin

    line_access: while (TT_string /= TT_EOF AND (not endfile(ihexfile))) loop

      -- *******************************
      -- Read Line
      line_str    := (others => ' ');                   -- empty the current string
      readline(ihexfile, ihexfile_line);                -- read line
      line_count  := line_count + 1;                    -- increase the line counter
      line_length := ihexfile_line'length;              -- get line length
      read(ihexfile_line, line_str(1 to line_length));  -- since the first ascii char is ':', we ignore it

      report LF & "[EX] ihex line " & integer'image(line_count) & " => " & line_str;

      -- *******************************
      -- Num of Bytes (LL):
      for i in 0 to 1 loop
        line_char := line_str(2+i);
        line_stdv := ascii_hex_2_stdv(line_char);
        LL_stdv(7-i*4 downto 4-i*4) := line_stdv;
      end loop;
      
      LL := to_integer(unsigned(LL_stdv));
      -- *******************************
      -- Start Address (AAAA):
      for i in 0 to 3 loop
        line_char := line_str(4+i);
        line_stdv := ascii_hex_2_stdv(line_char);
        AAAA_stdv(15-i*4 downto 12-i*4) := line_stdv;
      end loop;
      -- *******************************
      -- Record Field (TT):
      TT_string := line_str(8 to 9);

      if (TT_string = TT_EOF) then
        report LF & "[EX] TT (record type): End of Files" & LF severity note;
        exit line_access;
      end if;
      -- *******************************
      -- Data Field (DD):
      DD_loop: for i in 0 to LL-1 loop

        if (TT_string = TT_DATA) then   -- data record
          -- *******************************
          -- read data
          for j in 0 to 1 loop
            line_char := line_str(10+(i*2)+j);
            DD_stdv(7-j*4 downto 4-j*4) := ascii_hex_2_stdv(line_char);
          end loop;

          mem_line(7 downto 0) := DD_stdv;
          -- *******************************
          -- read address
          AAAA_lower  := std_logic_vector(unsigned(AAAA_stdv) + i);
          AAAA        := std_logic_vector(unsigned(AAAA_ext) + unsigned(AAAA_lower));
          mem_line(MEM_N-1 downto 8) := '1' & AAAA;
          -- *******************************
          -- mem line output
          mem(byte_count) := mem_line;
          byte_count      := byte_count + 1;

        else	-- extended address record => TT_string /= "00"
          for j in 0 to 1 loop
            line_char := line_str(10+(i*2)+j);
            if (i = 0) then
              AAAA_ext(31-j*4 downto 28-j*4) := ascii_hex_2_stdv(line_char);
            else
              AAAA_ext(23-j*4 downto 20-j*4) := ascii_hex_2_stdv(line_char);
            end if;
          end loop;

          if (i = LL-1) then  -- end of ext address
            AAAA_ext := ihex_tt(AAAA_ext , TT_string);

            assert (TT_string /= TT_ESA) report LF & "[EX] TT (record type): Extended Segment Address detected => 0x" & line_str(10 to 10+(LL*2)-1) & LF severity note;
            assert (TT_string /= TT_SSA) report LF & "[EX] TT (record type): Start Segment Address ignored => 0x" & line_str(10 to 10+(LL*2)-1) & LF severity warning;
            assert (TT_string /= TT_ELA) report LF & "[EX] TT (record type): Extended Linear Address detected => 0x" & line_str(10 to 10+(LL*2)-1) & LF severity note;
            assert (TT_string /= TT_SLA) report LF & "[EX] TT (record type): Start Linear Address ignored => 0x" & line_str(10 to 10+(LL*2)-1) & LF severity warning;
          end if;

        end if;

      end loop DD_loop;
    end loop line_access;

      report LF & "[EX] Line Count => " & integer'image(line_count) & LF severity note;
    report LF & "[EX] Byte Count => " & integer'image(byte_count) & LF severity note;

    return	mem;
  end function;

  -- ***************************************************************************
  -- Read a iHex TT field, if a extended segment address record type (or start)
  impure function ihex_tt (addr_in : in std_logic_vector; TT_string : in string) return std_logic_vector is
    variable  addr_out : std_logic_vector(31 downto 0) := (others => '0');
  begin
    -- *********************************
    if (TT_string = TT_ESA) then    -- extended segment address
      addr_out(31 downto 20) := (others => '0');
      addr_out(19 downto 4)  := addr_in(31 downto 16);
      addr_out(3 downto 0)   := (others => '0');
    -- *********************************
    elsif (TT_string = TT_SSA) then -- start segment address (ignored)
      null;
    -- *********************************
    elsif (TT_string = TT_ELA) then	-- extended linear address
      addr_out(31 downto 16):= addr_in(31 downto 16);
      addr_out(15 downto 0) := (others => '0');
    -- *********************************
    elsif (TT_string = TT_SLA) then	-- start linear address (ignored)
      null;
    -- *********************************
    else
      report LF & "[ERROR]: invalid TT (record type) detected" severity error;
    end if;
  
    return addr_out;
  end function;

-- *****************************************************************************
end package body ihex_init_pkg;
