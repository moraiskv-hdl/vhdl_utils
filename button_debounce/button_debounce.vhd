-- *****************************************************************************
-- Author:  Kevin Paula Morais
-- Date:    18/04/2019
-- E-mail:  moraiskv@gmail.com
-- *****************************************************************************
-- File:    Button Debounce
-- Description: a hardware solution for buttons
-- debounce, it is done by checking if the last
-- two states of the button (Q1 and Q2) are
-- stable in the same value for around 10 ms.
-- Then it is transferred the value of Q2 to
-- the output, if Q1 and Q2 are different,
-- then it means the mechanical contact still
-- not stable. The counter msb is used as overflow,
-- saying when the data is valid to be transferred
-- to Q3 (the output FF).
-- *****************************************************************************

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- *****************************************************************************
entity button_debounce is
  generic(
    N_COUNT_SIZE : integer := 22);
  port(
    clk_i   : in  std_logic;
    value_i : in  std_logic;
    value_o : out std_logic
  );
end entity button_debounce;

-- *****************************************************************************
architecture behavior of button_debounce is

  -- counter
  -- (2^22) ~= 4.194 M, so we use the msb as a overflow flag, which says when
  -- the count is done, at ~2.097M. With ~2.097M * 10nano seconds, we got ~20.97ms
  signal r_count : std_logic_vector(N_COUNT_SIZE-1 downto 0);

  -- temporary button ff
  signal r_q0 : std_logic;
  signal r_q1 : std_logic;
  signal r_q2 : std_logic := '0'; -- output have a known initial state

begin

  value_o <= r_q2;

  -- Button FF 1
  process(clk_i)
  begin
    if (rising_edge(clk_i)) then
      r_q0 <= value_i;
    end if;
  end process;

  -- Button FF 2
  process(clk_i)
  begin
    if (rising_edge(clk_i)) then
      r_q1 <= r_q0;
    end if;
  end process;

  -- Counter process
  process(clk_i)
  begin
    if (rising_edge(clk_i)) then
      if (r_q0 /= r_q1) then
        r_count <= (others => '0');
      elsif (r_count(N_COUNT_SIZE-1) = '0') then
        r_count <= std_logic_vector(unsigned(r_count) + 1);
      end if;
    end if;
  end process;

  -- Button output process
  process(clk_i)
  begin
    if (rising_edge(clk_i)) then
      if (r_count(N_COUNT_SIZE-1) = '1') then
        r_q2 <= r_q1;
      end if;
    end if;
  end process;

end architecture behavior;
