library ieee;
use ieee.std_logic_1164.all;

entity tb_button_debounce is
end entity tb_button_debounce;

-- *****************************************************************************
architecture behavior of tb_button_debounce is

  component button_debounce is
  port (
    clk_i   : in std_logic;
    value_i : in std_logic;
    value_o : out std_logic
  );
  end component button_debounce;

  constant HALF_PERIOD : time := 5 ns;  -- 100 MHz clock ; 10 ns period

  signal clk          : std_logic := '0';
  signal value_input  : std_logic := '0';
  signal bt_out       : std_logic;

begin

  -- clock generation
  process
  begin
    loop
    value_input <= NOT(value_input);
    wait for	 0.5 ms;

    value_input <= NOT(value_input);
    wait for	0.5 ms;

    value_input <= NOT(value_input);
    wait for	0.5 ms;

    value_input <= NOT(value_input);
    wait for	0.5 ms;

    value_input <= NOT(value_input);
    wait for	20 ms;
    ---------------
    end loop;
  end process;

  -- clock generation
  process
  begin
    loop
    clk	<= NOT(clk);
    wait for  HALF_PERIOD;

    clk <= NOT(clk);
    wait for HALF_PERIOD;
    end loop;
  end process;

  -- port map
  i_button_debounce: button_debounce
  port map (
    clk_i   => clk,
    value_i	=> value_input,
    value_o	=> value_output
  );

end architecture behavior;
