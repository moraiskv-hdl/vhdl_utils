# VHDL Utils

This repository contains a set of VHDL codes, useful in common situations. For example, a push button debounce logic, or a package with functions to read a intel hexadecimal file, so we can start a given memory with desired values.
